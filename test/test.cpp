// testmempool.cpp 
//

#include "../pool/interface.h"
#if (defined(_MSC_VER))
#ifdef _WIN64
#pragma comment(lib, "./x64/Debug/mempool.lib")
#else
#pragma comment(lib, "./Debug/mempool.lib")
#endif
#endif

int main()
{
	init_memory_pool("test123", 0x100000, (void*)0x50000000);

	char *pch1 = nullptr, *pch2 = nullptr, *pch3 = nullptr, *pch4 = nullptr, *pch5 = nullptr, *pch6 = nullptr;
	allocate_buffer(4096, MEM_SHARE, (void**)&pch1);
	*pch1 = 'A';

	allocate_buffer(4096, MEM_SHARE, (void**)&pch2);
	*pch2 = 'B';

	allocate_buffer(1000, MEM_SHARE, (void**)&pch3);
	*pch3 = 'C';

	allocate_buffer(4096, MEM_SHARE, (void**)&pch4);
	*pch4 = 'D';

	allocate_more(5, pch1, (void**)&pch5);
	*pch5 = 'E';
	free_buffer(pch1);
	free_buffer(pch3);
	free_buffer(pch2);

	allocate_buffer(5, MEM_SHARE, (void**)&pch6);
	*pch6 = 'F';

	destoryt_memory_pool();
    return 0;
}

