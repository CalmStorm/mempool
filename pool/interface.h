﻿//      Copyright (c) ssselice@126.com. All rights reserved.
#ifndef POOL_INTERFACE_H_
#define POOL_INTERFACE_H_

#include <stdint.h>

#if (defined(__linux) || defined(__linux__))
#define MEMPOOL_API __attribute__((visibility("default")))
#else   // windows
#ifdef _MEMPOOL_EXP
#define MEMPOOL_API    _declspec(dllexport)
#else
#define MEMPOOL_API    _declspec(dllimport)
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(push, 1)

#define MEM_SHARE                        0x00000001        // 在共享区创建
#define MEM_ZEROINIT                     0x00000002        // 初始化为0

#define MEM_SUCCESS                             (0)
#define MEM_ERR_INVALID_VALUE                   (-8)
#define MEM_ERR_INVALID_BUFFER                  (-18)
#define MEM_ERR_INVALID_POINTER                 (-26)
#define MEM_ERR_INVALID_SIZE                    (-34)
#define MEM_ERR_OUT_OF_MEMORY                   (-42)

// 初始化共享内存池
// mempool_name: 共享空间名称,用于唯一初始化    mempool_size: 共享空间大小 lpBaseAddress: 映射基本地址
int32_t MEMPOOL_API init_memory_pool(const char* mempool_name, int64_t mempool_size, void* lpv_base_address);

// 申请内存空间
// alloc_size: 申请空间的字节大小  flags: 申请的内存类型和初始化参数    lppvdata: 输出参数，内存地址
int32_t MEMPOOL_API allocate_buffer(uint32_t alloc_size, uint32_t flags, void** lppv_data);

// 申请更多的内存空间，形成链表
// alloc_size: 申请空间的字节大小  lpv_original: 链表头指针    lppv_data: 输出参数，内存地址
int32_t MEMPOOL_API allocate_more(uint32_t alloc_size, void* lpv_original, void** lppv_data);

// 释放内存空间链表
// lpv_data: 内存链表头指针
int32_t MEMPOOL_API free_buffer(void* lpv_data);

// 释放共享空间
int32_t MEMPOOL_API destoryt_memory_pool();

#pragma pack(pop)

#ifdef __cplusplus
}       /*extern "C"*/
#endif

#endif  // POOL_INTERFACE_H_
