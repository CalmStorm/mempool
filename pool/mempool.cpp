﻿//      Copyright (c) ssselice@126.com. All rights reserved.
#include "mempool.h"
#if (defined(__linux) || defined(__linux__))
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#else
#include <Windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>

#ifndef max
#define max(a, b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a, b) (((a) < (b)) ? (a) : (b))
#endif

// 存储共享空间相关数据
share_memory_info_t global_share_memory_info;
// 共享空间句柄
share_memory_t global_share_memory;
// 页面的粒度
uint32_t global_page_size = 0;
// 起始偏移量
uint32_t global_memory_pool_offset = 0, global_memory_pool_max_count = 0;

bool share_memory_t::lock()
{
    if (mutex_lock == nullptr)
        return false;
#if (defined(__linux) || defined(__linux__))
    return CEvent::WAIT_OBJECT_0 == CEvent::WaitForSingleObject(mutex_lock, 0);
#else
    return WAIT_OBJECT_0 == ::WaitForSingleObject(mutex_lock, INFINITE);
#endif
}

bool share_memory_t::unlock()
{
    if (mutex_lock == nullptr)
        return false;
#if (defined(__linux) || defined(__linux__))
    return CEvent::ResetEvent(mutex_lock);
#else
    return ::ReleaseMutex(mutex_lock) == TRUE;
#endif
}

void share_memory_t::free()
{
    if (map_view_buffer != nullptr)
    {
#if (defined(__linux) || defined(__linux__))
        shmdt(map_view_buffer);
#else
        ::UnmapViewOfFile(map_view_buffer);
#endif
        map_view_buffer = nullptr;
    }
#if (defined(__linux) || defined(__linux__))
    if (shmid != 0)
    {
        shmctl(shmid, IPC_RMID, NULL);
        shmid = 0;
    }
#else
    if (file_mapping != nullptr)
    {
        ::CloseHandle(file_mapping);
        file_mapping = nullptr;
    }
#endif
    if (mutex_lock != nullptr)
    {
#if (defined(__linux) || defined(__linux__))
        CEvent::ResetEvent(mutex_lock);
        CEvent::CloseHandle(mutex_lock);
#else
        ::ReleaseMutex(mutex_lock);
        ::CloseHandle(mutex_lock);
        mutex_lock = nullptr;
#endif
    }
}

share_memory_t::~share_memory_t()
{
    free();
}

///
uint32_t next_pow2(uint32_t v)
{
    --v;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    return ++v;
}
///
int32_t init_memory_pool(const char *mempool_name, int64_t mempool_size, void *lpv_base_address)
{
    // 清空共享空间参数
    memset(&global_share_memory_info, 0, sizeof(share_memory_info_t));
    memset(static_cast<void *>(&global_share_memory), 0, sizeof(share_memory_t));

    // 检查共享空间名称
    size_t mempool_name_len = strnlen(mempool_name, MEMPOOL_NAME_LEN);
    if (mempool_name_len == 0)
        return MEM_ERR_INVALID_POINTER;

// 检查地址和长度
#if (defined(__linux) || defined(__linux__))
    global_page_size = getpagesize();
#else
    SYSTEM_INFO system_info;
    GetSystemInfo(&system_info);
    // 每个内存池的大小位一个页面
    global_page_size = system_info.dwPageSize;
    if (lpv_base_address != nullptr)
    {
        if (lpv_base_address < system_info.lpMinimumApplicationAddress ||
            lpv_base_address > system_info.lpMaximumApplicationAddress)
            return MEM_ERR_INVALID_POINTER;
        else if (((int64_t)system_info.lpMaximumApplicationAddress - (int64_t)lpv_base_address) < mempool_size)
            return MEM_ERR_INVALID_SIZE;
    }
#endif
    memcpy(global_share_memory_info.mempool_name, mempool_name, min(mempool_name_len, MEMPOOL_NAME_LEN));
    global_share_memory_info.mempool_size = mempool_size;

    // 检查共享空间大小
    if (mempool_size < MEMPOOL_MIN_MEMORY_SIZE)
        return MEM_ERR_INVALID_SIZE;
    // 创建互斥量
    char mutex_name[MEMPOOL_NAME_LEN + 24] = {0};
    snprintf(mutex_name, sizeof(mutex_name), "mutex_%s", global_share_memory_info.mempool_name);
#if (defined(__linux) || defined(__linux__))
    global_memory_share.mutex_lock = CEvent::CreateEvent(0, true, false, mutex_name);
    if (global_memory_share.mutex_lock == nullptr)
    {
        // 创建互斥体失败
        return MEM_ERR_INVALID_VALUE;
    }
#else
    global_share_memory.mutex_lock = ::CreateMutexA(nullptr, FALSE, mutex_name);
    if (global_share_memory.mutex_lock == nullptr || global_share_memory.mutex_lock == INVALID_HANDLE_VALUE)
    {
        // 创建互斥体失败
        return MEM_ERR_INVALID_VALUE;
    }
#endif
    // 获取互斥体
    if (!global_share_memory.lock())
    {
        global_share_memory.free();
        return MEM_ERR_INVALID_VALUE;
    }
#if (defined(__linux) || defined(__linux__))
    key_t key = ftok(reinterpret_cast<char *>(global_memory_share_info.mempool_name), 9999);

    global_memory_share.shmid = shmget(key, global_memory_share_info.mempool_size, IPC_CREAT | SHM_R | SHM_W | 0666);
    if (global_memory_share.shmid == -1)
    {
        // 创建文件映射内核对象错误
        global_memory_share.free();
        return MEM_ERR_INVALID_VALUE;
    }
    global_memory_share.map_view_buffer = shmat(global_memory_share.shmid, lpv_base_address, 0);
    if (global_memory_share.map_view_buffer == nullptr || (int64_t)(global_memory_share.map_view_buffer) == -1)
    {
        // 创建文件映射内核对象错误
        global_memory_share.free();
        return MEM_ERR_INVALID_VALUE;
    }
    shmid_ds memory_ds = {0};
    if (0 == shmctl(global_memory_share.shmid, IPC_STAT, &memory_ds))
        global_memory_share_info.mempool_size = memory_ds.shm_segsz;
#else
    // 根据名称创建FileMapping
    global_share_memory.file_mapping =
        ::CreateFileMappingA(INVALID_HANDLE_VALUE, nullptr, PAGE_READWRITE,
                             HIUINT32(global_share_memory_info.mempool_size),
                             LOUINT32(global_share_memory_info.mempool_size),
                             reinterpret_cast<char *>(global_share_memory_info.mempool_name));
    if (global_share_memory.file_mapping == nullptr || global_share_memory.file_mapping == INVALID_HANDLE_VALUE)
    {
        // 创建文件映射内核对象错误
        global_share_memory.free();
        return MEM_ERR_INVALID_VALUE;
    }

    // 将对象映射到给定的内存地址
    global_share_memory.map_view_buffer = ::MapViewOfFileEx(global_share_memory.file_mapping,
                                                            FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0, lpv_base_address);
    if (global_share_memory.map_view_buffer == nullptr || global_share_memory.map_view_buffer == INVALID_HANDLE_VALUE)
    {
        // 创建映射失败
        global_share_memory.free();
        return MEM_ERR_INVALID_VALUE;
    }
    MEMORY_BASIC_INFORMATION mbi = {0};
    if (0 < VirtualQuery(global_share_memory.map_view_buffer, &mbi, sizeof(MEMORY_BASIC_INFORMATION)))
        global_share_memory_info.mempool_size = mbi.RegionSize;
#endif
    int32_t pool_count_per_page = global_page_size / sizeof(memory_pool_node_t);
    global_memory_pool_offset = LOUINT32(((global_share_memory_info.mempool_size) / global_page_size + pool_count_per_page - 1) / pool_count_per_page + 1);
    global_memory_pool_max_count = min(global_memory_pool_offset * global_page_size / sizeof(memory_pool_node_t), INT32_MAX & 0xFFFFFF);
    global_share_memory.unlock();
    return MEM_SUCCESS;
}
// 初始化内存池及内存区域
bool internal_init_memory_pool_and_block(int32_t index, uint32_t memory_block_size, int32_t memory_pool_node_count)
{
    memory_pool_node_t *p_memory_pool_node = reinterpret_cast<memory_pool_node_t *>(global_share_memory.map_view_buffer) + index;
    if (memory_block_size >= global_page_size)
    {
        if ((int64_t)index * global_page_size + memory_block_size > global_share_memory_info.mempool_size)
            return false;
        memset(p_memory_pool_node, 0, sizeof(memory_pool_node_t) * memory_pool_node_count);
        memory_block_t *current_block = reinterpret_cast<memory_block_t *>(reinterpret_cast<int8_t *>(global_share_memory.map_view_buffer) + index * global_page_size);
        p_memory_pool_node->memory_block_size = memory_block_size;
        p_memory_pool_node->max_memory_block_count = 1;
        p_memory_pool_node->left_memory_block_count = 1;
        p_memory_pool_node->p_head = current_block;
        p_memory_pool_node->p_idle = current_block;
        // 更新内存块数据
        memset(current_block, 0, memory_block_size);
        current_block->p_pre = current_block;
        current_block->p_next = current_block;
        current_block->used = 0;
        current_block->memory_pool_index = index;
    }
    else
    {
        if ((int64_t)(index + 1) * global_page_size > global_share_memory_info.mempool_size)
            return false;
        memset(p_memory_pool_node, 0, sizeof(memory_pool_node_t) * memory_pool_node_count);
        int memory_block_count = global_page_size / memory_block_size;
        memory_block_t *current_block = reinterpret_cast<memory_block_t *>(reinterpret_cast<int8_t *>(global_share_memory.map_view_buffer) + index * global_page_size);
        p_memory_pool_node->memory_block_size = memory_block_size;
        p_memory_pool_node->max_memory_block_count = memory_block_count;
        p_memory_pool_node->left_memory_block_count = memory_block_count;
        p_memory_pool_node->p_head = current_block;
        p_memory_pool_node->p_idle = current_block;
        memset(current_block, 0, global_page_size);
        for (int memory_block_index = 0; memory_block_index < memory_block_count; memory_block_index++)
        {
            current_block = reinterpret_cast<memory_block_t *>(reinterpret_cast<int8_t *>(p_memory_pool_node->p_head) + memory_block_size * memory_block_index);
            current_block->p_pre = current_block;
            current_block->p_next = current_block;
            current_block->used = 0;
            current_block->memory_pool_index = index;
        }
    }
    return true;
}

bool internal_alloc_memory_from_pool(int64_t index, void **lppv_data)
{
    memory_pool_node_t *p_memory_pool_node = reinterpret_cast<memory_pool_node_t *>(global_share_memory.map_view_buffer) + index;

    if (p_memory_pool_node->left_memory_block_count <= 0)
        return false;
    p_memory_pool_node->left_memory_block_count--;

    memory_block_t *current_block = p_memory_pool_node->p_idle;
    current_block->used = 1;
    current_block->p_next = nullptr;
    current_block->p_pre = current_block;
    *lppv_data = reinterpret_cast<int8_t *>(current_block) + sizeof(memory_block_t);
    // 查找下一个可用的空间
    if (p_memory_pool_node->left_memory_block_count > 0)
    {
        int16_t max_memory_block_count = p_memory_pool_node->max_memory_block_count;
        int16_t cur_memory_block_index = LOUINT16((reinterpret_cast<int8_t *>(current_block) -
                                                   reinterpret_cast<int8_t *>(p_memory_pool_node->p_head)) /
                                                  p_memory_pool_node->memory_block_size);
        while (current_block->used == 1)
        {
            if (cur_memory_block_index + 1 >= max_memory_block_count)
            {
                cur_memory_block_index = 0;
                current_block = p_memory_pool_node->p_head;
            }
            else
            {
                cur_memory_block_index++;
                current_block = reinterpret_cast<memory_block_t *>(reinterpret_cast<int8_t *>(current_block) + p_memory_pool_node->memory_block_size);
            }
        }
        p_memory_pool_node->p_idle = current_block;
    }
    else
    {
        p_memory_pool_node->p_idle = nullptr;
    }
    return true;
}

bool valid_memory_block_pointer(void *lpv_original)
{
    if ((uint64_t)lpv_original < (uint64_t)global_share_memory.map_view_buffer ||
        (uint64_t)lpv_original > ((uint64_t)global_share_memory.map_view_buffer + global_share_memory_info.mempool_size))
    {
        return false;
    }
    return true;
}

int32_t internal_allocate_buffer(uint32_t alloc_size, void **lppv_data)
{
    *lppv_data = nullptr;
    memory_pool_node_t *p_memory_pool_node = reinterpret_cast<memory_pool_node_t *>(global_share_memory.map_view_buffer);
    uint32_t index_memory_pool = global_memory_pool_offset;
    // 指示是否查找到内存池节点
    bool b_found_memory_pool = false;
    // 申请的空间要加上内存节点的空间
    alloc_size += sizeof(memory_block_t);
    // 向上取2的幂整数
    uint32_t alloc_memory_block_size = next_pow2(alloc_size);
    if (alloc_memory_block_size < global_page_size)
    {
        // 小于一个页面的情况，遍历各个内存池节点，查找可用内存池节点
        // 在一个内存池中,先寻找可用的内存池，如果没找到，再新建
        index_memory_pool = global_memory_pool_offset;
        while (index_memory_pool < global_memory_pool_max_count)
        {
            if (p_memory_pool_node[index_memory_pool].memory_block_size == 0)
                break;
            if (p_memory_pool_node[index_memory_pool].memory_block_size < 0)
            {
                // 此时代表该空间为空闲内存池节点，将节点类型转变为空闲内存池，然后跳过
                index_memory_pool += abs(reinterpret_cast<idle_memory_pool_node_t *>(&p_memory_pool_node[index_memory_pool])->count);
                continue;
            }
            if (p_memory_pool_node[index_memory_pool].memory_block_size == (int32_t)alloc_memory_block_size &&
                p_memory_pool_node[index_memory_pool].left_memory_block_count > 0)
            {
                b_found_memory_pool = true;
                break;
            }
            index_memory_pool++;
        }
        if (b_found_memory_pool)
        {
            // 找到了可用的空间
            if (internal_alloc_memory_from_pool(index_memory_pool, lppv_data))
            {
                global_share_memory.unlock();
                return MEM_SUCCESS;
            }
            b_found_memory_pool = false;
        }
    }
    else
    {
        // 大于一个页面的情况，与页面的整数倍
        // 如果申请的空间超出一个页面的范围,意味着没有可用的内存池,必须申请新的内存池
        alloc_memory_block_size = (alloc_size + global_page_size - 1) / global_page_size * global_page_size;
    }
    // 计算需要的内存池节点数量
    int alloc_memory_pool_node_count = (alloc_memory_block_size + global_page_size - 1) / global_page_size;

    // 先从空闲内存池链表中找一个可用的，如果没找到，再从最后面规划一个
    idle_memory_pool_node_t *p_idle_memory_pool_node_header = reinterpret_cast<idle_memory_pool_node_t *>(global_share_memory.map_view_buffer);
    if (p_idle_memory_pool_node_header->p_next != nullptr)
    {
        // 从可用内存池里找一个
        // 查找大小够用的内存池节点
        idle_memory_pool_node_t *p_idle_memory_pool_node = p_idle_memory_pool_node_header->p_next;
        while (p_idle_memory_pool_node != nullptr && p_idle_memory_pool_node != p_idle_memory_pool_node_header)
        {
            if (abs(p_idle_memory_pool_node->count) >= alloc_memory_pool_node_count)
            {
                b_found_memory_pool = true;
                break;
            }
            p_idle_memory_pool_node = p_idle_memory_pool_node->p_next;
        }
        if (b_found_memory_pool)
        {
            // 找到了大小够用的内存池
            if (abs(p_idle_memory_pool_node->count) > alloc_memory_pool_node_count)
            {
                // 1、该空闲内存池节点大小空间有剩余
                // 从该内存池节点的后面取需要的空间数量，然后修改节点大小即可
                p_idle_memory_pool_node->count += alloc_memory_pool_node_count; // 因为负数表示，所以用加法
                index_memory_pool = p_idle_memory_pool_node->index + abs(p_idle_memory_pool_node->count);
            }
            else
            {
                // 2、该空闲内存池节点大小空间正好
                index_memory_pool = p_idle_memory_pool_node->index;
                // 将该节点从链表中删除
                p_idle_memory_pool_node->p_pre->p_next = p_idle_memory_pool_node->p_next;
                p_idle_memory_pool_node->p_next->p_pre = p_idle_memory_pool_node->p_pre;
            }
        }
    }
    if (!b_found_memory_pool)
    {
        // 没找到空闲内存池或者空闲内存池为空, 从头开始查找
        index_memory_pool = global_memory_pool_offset;
        while (index_memory_pool < global_memory_pool_max_count)
        {
            if (p_memory_pool_node[index_memory_pool].memory_block_size == 0)
            {
                b_found_memory_pool = true;
                break;
            }
            if (p_memory_pool_node[index_memory_pool].memory_block_size < 0)
            {
                // 此时代表该空间为空闲内存池节点，将节点类型转变为空闲内存池，然后跳过
                index_memory_pool += abs(reinterpret_cast<idle_memory_pool_node_t *>(&p_memory_pool_node[index_memory_pool])->count);
                continue;
            }
            else
            {
                // 计算节点所占用空间
                int memory_pool_node_count = (p_memory_pool_node[index_memory_pool].memory_block_size + global_page_size - 1) / global_page_size;
                index_memory_pool += memory_pool_node_count;
            }
        }
    }
    if (!b_found_memory_pool)
    {
        // 没找到可用内存池
        return MEM_ERR_OUT_OF_MEMORY;
    }
    // 初始化内存池
    if (!internal_init_memory_pool_and_block(index_memory_pool, alloc_memory_block_size, alloc_memory_pool_node_count))
    {
        return MEM_ERR_OUT_OF_MEMORY;
    }
    if (!internal_alloc_memory_from_pool(index_memory_pool, lppv_data))
    {
        return MEM_ERR_OUT_OF_MEMORY;
    }
    return MEM_SUCCESS;
}

int32_t allocate_buffer(uint32_t alloc_size, uint32_t flags, void **lppv_data)
{
    if (!global_share_memory.lock())
    {
        // 无法锁定操作
        return MEM_ERR_INVALID_POINTER;
    }
    int32_t ret = MEM_SUCCESS;
    *lppv_data = nullptr;
    if ((flags & MEM_SHARE) == 0)
    {
        *lppv_data = malloc(alloc_size);
        if (*lppv_data != nullptr && (flags & MEM_ZEROINIT))
            memset(*lppv_data, 0, alloc_size);
        if (*lppv_data == nullptr)
            ret = MEM_ERR_OUT_OF_MEMORY;
    }
    else
    {
        ret = internal_allocate_buffer(alloc_size, lppv_data);
    }
    global_share_memory.unlock();
    return ret;
}

int32_t allocate_more(uint32_t alloc_size, void *lpv_original, void **lppv_data)
{
    if (!global_share_memory.lock())
    {
        // 无法锁定操作
        return MEM_ERR_INVALID_POINTER;
    }
    // 查找原始指针
    int32_t ret = MEM_SUCCESS;
    memory_block_t *original_block = nullptr;
    // 检查原始指针是否在共享区域
    if (!valid_memory_block_pointer(lpv_original))
        ret = MEM_ERR_INVALID_POINTER;

    if (ret == MEM_SUCCESS)
    {
        // 查找原始指针指向的链表最后一个指针
        original_block = reinterpret_cast<memory_block_t *>(reinterpret_cast<int8_t *>(lpv_original) - sizeof(memory_block_t));
        if (valid_memory_block_pointer(original_block->p_pre) &&
            (original_block->p_next == nullptr || valid_memory_block_pointer(original_block->p_next)))
        {
            while (original_block->p_next != nullptr)
            {
                original_block = original_block->p_next;
                if (!valid_memory_block_pointer(original_block))
                {
                    ret = MEM_ERR_INVALID_POINTER;
                    break;
                }
            }
        }
        else
        {
            ret = MEM_ERR_INVALID_POINTER;
        }
    }
    if (ret == MEM_SUCCESS)
    {
        ret = internal_allocate_buffer(alloc_size, lppv_data);
    }

    if (ret == MEM_SUCCESS)
    {
        // 修复指针
        original_block->p_next = reinterpret_cast<memory_block_t *>(reinterpret_cast<int8_t *>(*lppv_data) - sizeof(memory_block_t));
        original_block->p_next->p_pre = original_block;
    }
    global_share_memory.unlock();
    return ret;
}

int32_t free_buffer(void *lpv_data)
{
    if (!global_share_memory.lock())
    {
        // 无法锁定操作
        return MEM_ERR_INVALID_POINTER;
    }
    int32_t ret = MEM_SUCCESS;
    if (lpv_data == nullptr)
    {
        ret = MEM_ERR_INVALID_POINTER;
    }
    else
    {
        if (!valid_memory_block_pointer(lpv_data))
        {
            // 没在共享内存区域，直接释放
            free(lpv_data);
        }
        else
        {
            // 释放内存块, 修改上级指针
            memory_block_t *current_block = reinterpret_cast<memory_block_t *>(reinterpret_cast<int8_t *>(lpv_data) - sizeof(memory_block_t));
            if (current_block->p_pre != current_block)
            {
                // 断开链表
                current_block->p_pre->p_next = nullptr;
            }
            // 从当前链表开始释放
            memory_pool_node_t *p_memory_pool_node = reinterpret_cast<memory_pool_node_t *>(global_share_memory.map_view_buffer);
            memory_block_t *next_block = current_block;
            while (next_block != nullptr)
            {
                current_block = next_block;
                next_block = current_block->p_next;
                // 释放当前内存块节点
                current_block->used = 0;
                uint32_t memory_pool_index = current_block->memory_pool_index;
                p_memory_pool_node[memory_pool_index].left_memory_block_count++;
                // 计算当前节点所属内存池是否全部释放
                if (p_memory_pool_node[memory_pool_index].left_memory_block_count ==
                    p_memory_pool_node[memory_pool_index].max_memory_block_count)
                {
                    // 全部释放了，将当前内存池借钱清空，并添加到空闲内存池节点链表
                    int memory_pool_node_count = (p_memory_pool_node[memory_pool_index].memory_block_size + global_page_size - 1) / global_page_size;

                    // 清空内存块区域，以防止指针被乱用
                    memset(p_memory_pool_node[memory_pool_index].p_head, 0,
                           p_memory_pool_node[memory_pool_index].memory_block_size * p_memory_pool_node[memory_pool_index].max_memory_block_count);

                    // 当前内存池节点转换为空闲内存池节点
                    idle_memory_pool_node_t *p_idle_memory_pool_node = reinterpret_cast<idle_memory_pool_node_t *>(&p_memory_pool_node[memory_pool_index]);
                    memset(p_idle_memory_pool_node, 0, sizeof(idle_memory_pool_node_t) * memory_pool_node_count);
                    p_idle_memory_pool_node->count = -memory_pool_node_count;
                    p_idle_memory_pool_node->index = memory_pool_index;

                    // 查找该空闲内存池节点和合适位置
                    idle_memory_pool_node_t *idle_memory_pool_node_header = reinterpret_cast<idle_memory_pool_node_t *>(global_share_memory.map_view_buffer);
                    idle_memory_pool_node_t *current_idle_memory_pool_node = idle_memory_pool_node_header->p_next;
                    bool b_found_memory_pool_node_pos = false;
                    while (current_idle_memory_pool_node != nullptr && current_idle_memory_pool_node != idle_memory_pool_node_header)
                    {
                        if (current_idle_memory_pool_node->index > p_idle_memory_pool_node->index)
                        {
                            b_found_memory_pool_node_pos = true;
                            break;
                        }
                        current_idle_memory_pool_node = current_idle_memory_pool_node->p_next;
                    }

                    if (b_found_memory_pool_node_pos)
                    {
                        // 找到了位置
                        idle_memory_pool_node_t *pre_idle_memory_pool_node = current_idle_memory_pool_node->p_pre;
                        if ((pre_idle_memory_pool_node->index + abs(pre_idle_memory_pool_node->count) ==
                             p_idle_memory_pool_node->index) &&
                            (p_idle_memory_pool_node->index + abs(p_idle_memory_pool_node->count) ==
                             current_idle_memory_pool_node->index))
                        {
                            // 与前面和后面的同时合并
                            pre_idle_memory_pool_node->count +=
                                p_idle_memory_pool_node->count + current_idle_memory_pool_node->count;
                            pre_idle_memory_pool_node->p_next = current_idle_memory_pool_node->p_next;
                            current_idle_memory_pool_node->p_next->p_pre = pre_idle_memory_pool_node;
                            memset(reinterpret_cast<int8_t *>(pre_idle_memory_pool_node) + sizeof(idle_memory_pool_node_t), 0,
                                   sizeof(idle_memory_pool_node_t) * (abs(pre_idle_memory_pool_node->count) - 1));
                        }
                        else if (pre_idle_memory_pool_node->index + abs(pre_idle_memory_pool_node->count) ==
                                 p_idle_memory_pool_node->index)
                        {
                            // 仅与前面的节点合并
                            pre_idle_memory_pool_node->count += p_idle_memory_pool_node->count;
                            memset(reinterpret_cast<int8_t *>(pre_idle_memory_pool_node) + sizeof(idle_memory_pool_node_t), 0,
                                   sizeof(idle_memory_pool_node_t) * (abs(pre_idle_memory_pool_node->count) - 1));
                        }
                        else if (p_idle_memory_pool_node->index + abs(p_idle_memory_pool_node->count) ==
                                 current_idle_memory_pool_node->index)
                        {
                            // 仅与后面的节点合并
                            p_idle_memory_pool_node->count += current_idle_memory_pool_node->count;
                            pre_idle_memory_pool_node->p_next = p_idle_memory_pool_node;
                            current_idle_memory_pool_node->p_next->p_pre = p_idle_memory_pool_node;
                            p_idle_memory_pool_node->p_next = current_idle_memory_pool_node->p_next;
                            p_idle_memory_pool_node->p_pre = current_idle_memory_pool_node->p_pre;
                            memset(reinterpret_cast<int8_t *>(p_idle_memory_pool_node) + sizeof(idle_memory_pool_node_t), 0,
                                   sizeof(idle_memory_pool_node_t) * (abs(p_idle_memory_pool_node->count) - 1));
                        }
                        else
                        {
                            // 仅插入
                            pre_idle_memory_pool_node->p_next = p_idle_memory_pool_node;
                            current_idle_memory_pool_node->p_pre = p_idle_memory_pool_node;
                            p_idle_memory_pool_node->p_next = current_idle_memory_pool_node;
                            p_idle_memory_pool_node->p_pre = pre_idle_memory_pool_node;
                        }
                    }
                    else
                    {
                        // 没找到位置,放在最后，需要看一下是否要和前面的合并
                        idle_memory_pool_node_t *tail_idle_memory_pool_node = idle_memory_pool_node_header->p_pre;
                        if (tail_idle_memory_pool_node == nullptr)
                        {
                            idle_memory_pool_node_header->p_next = p_idle_memory_pool_node;
                            idle_memory_pool_node_header->p_pre = p_idle_memory_pool_node;
                            p_idle_memory_pool_node->p_next = idle_memory_pool_node_header;
                            p_idle_memory_pool_node->p_pre = idle_memory_pool_node_header;
                        }
                        else
                        {
                            if (tail_idle_memory_pool_node->index + abs(tail_idle_memory_pool_node->count) ==
                                p_idle_memory_pool_node->index)
                            {
                                tail_idle_memory_pool_node->count += tail_idle_memory_pool_node->count;
                                // 后面的清空
                                memset(reinterpret_cast<int8_t *>(tail_idle_memory_pool_node) + sizeof(idle_memory_pool_node_t), 0,
                                       (abs(tail_idle_memory_pool_node->count) - 1) * sizeof(idle_memory_pool_node_t));
                            }
                            else
                            {
                                // 否则加到链表后面
                                p_idle_memory_pool_node->p_pre = tail_idle_memory_pool_node;
                                p_idle_memory_pool_node->p_next = tail_idle_memory_pool_node->p_next;
                                tail_idle_memory_pool_node->p_next->p_pre = p_idle_memory_pool_node;
                                tail_idle_memory_pool_node->p_next = p_idle_memory_pool_node;
                            }
                        }
                    }
                }
                else if (p_memory_pool_node[memory_pool_index].p_idle == nullptr)
                {
                    p_memory_pool_node[memory_pool_index].p_idle = current_block;
                }
                else
                {
                    current_block->p_pre = current_block;
                    current_block->p_next = current_block;
                }
            }
        }
    }
    global_share_memory.unlock();
    return ret;
}

int32_t destoryt_memory_pool()
{
    global_share_memory.free();
    return MEM_SUCCESS;
}
