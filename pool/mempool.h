﻿//      Copyright (c) ssselice@126.com. All rights reserved.
#ifndef POOL_MEMPOOL_H_
#define POOL_MEMPOOL_H_

#include "interface.h"
#if (defined(__linux) || defined(__linux__))
#include "Event.h"
#endif
#pragma pack(push, 1)

#define MEMPOOL_NAME_LEN 64
#define MEMPOOL_MIN_MEMORY_SIZE 0x7000

#define LOUINT32(l) ((uint32_t)(((int64_t)(l)) & 0xffffffff))
#define HIUINT32(l) ((uint32_t)((((int64_t)(l)) >> 32) & 0xffffffff))
#define LOUINT16(l) ((uint16_t)(((int32_t)(l)) & 0xffff))
#define HIUINT16(l) ((uint16_t)((((int32_t)(l)) >> 16) & 0xffff))

// 存放共享内存相关数据
struct share_memory_info_t {
    int8_t mempool_name[MEMPOOL_NAME_LEN + 1];
    int64_t mempool_size;
};

// 存放共享内存空间句柄及互斥锁
struct share_memory_t {
#if (defined(__linux) || defined(__linux__))
    CEvent* mutex_lock;
    int shmid;
#else
    void *mutex_lock;
    void *file_mapping;
#endif
    void *map_view_buffer;
    bool lock();
    bool unlock();
    void free();
    ~share_memory_t();
};
// 内存块节点（24字节）
struct memory_block_t {
    union {
        int8_t reserved[24];                        // 对齐到24字节
        struct {
            memory_block_t *p_pre;                  // 前一个节点指针
            uint8_t used;                           // 所属内存池节点是否使用
            memory_block_t *p_next;                 // 下一个节点指针
            uint32_t memory_pool_index;             // 所属内存池节点索引
        };
    };
    //    int8_t data[0];
};

// 内存池节点（24字节）, 每一个内存池纸少有一个页
struct memory_pool_node_t {
    int32_t memory_block_size;                      // 内存池中的内存块节点大小
    int16_t max_memory_block_count;                 // 内存池中的内存块节点总数
    int16_t left_memory_block_count;                // 剩余内存池中的内存块节点数量
    union {
        memory_block_t *p_head;                     // 内存块节点首地址
        int8_t reserved1[8];                        // 用于内存对齐
    };
    union {
        memory_block_t *p_idle;                     // 下一个空闲内存块节点
        int8_t reserved2[8];                        // 用于内存对齐
    };
};

// 空闲的内存池节点(24节点), 链表是顺序存储的，以便从中间插入
struct idle_memory_pool_node_t {
    int32_t count;                                  // 负数表示 空闲内存池节点数量，包含当前节点和后面的节点
    int32_t index;                                  // 当前空闲内存池所属节点索引
    union {
        idle_memory_pool_node_t *p_next;            // 下一个空闲内存池节点
        int8_t reserved1[8];                        // 用于内存对齐
    };
    union {
        idle_memory_pool_node_t *p_pre;             // 上一个空闲内存池节点
        int8_t reserved2[8];                        // 用于内存对齐
    };
};

#pragma pack(pop)

#endif  // POOL_MEMPOOL_H_
